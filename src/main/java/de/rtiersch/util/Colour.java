/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.util;

import javafx.scene.paint.Color;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

/**
 * Colour utility class. 'Cause it isn't already there...
 */
public final class Colour
{
    /**
     * Instance for intern data.
     */
    private static final Colour INSTANCE = new Colour();
    /**
     * Max decimal value of two hex digits.
     */
    private static final double MAX_HEX  = 255;

    /**
     * Mapping for faster Color code finding. If it is faster. Well, I assume.
     * Java is using a Hashmap for that...
     */
    private final HashMap<Color, String> m_map = new HashMap<>();

    /**
     * Utility class. No public instances.
     */
    private Colour()
    {

    }

    /**
     * Get the CSS string for that colour.
     *
     * @param p_colour Colour to extract css value.
     * @return #rrggbb
     */
    @NotNull
    public static String getCSS(@NotNull final Color p_colour)
    {
        final String l_s = Colour.INSTANCE.m_map.get(p_colour);
        if (l_s != null) {
            return l_s;
        }
        final double l_red = p_colour.getRed();
        final double l_green = p_colour.getGreen();
        final double l_blue = p_colour.getBlue();
        final int l_r = (int) Math.round(l_red * MAX_HEX);
        final int l_g = (int) Math.round(l_green * MAX_HEX);
        final int l_b = (int) Math.round(l_blue * MAX_HEX);
        final String l_string = String.format("#%02x%02x%02x", l_r, l_g, l_b);
        Colour.INSTANCE.m_map.put(p_colour, l_string);
        return l_string;
    }
}
