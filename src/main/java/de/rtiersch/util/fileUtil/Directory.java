/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.util.fileUtil;

import org.apache.commons.io.FilenameUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.net.URI;

public class Directory
        extends File {

    protected boolean m_isValid = true;

    /**
     * Creates a new {@code File} instance by converting the given pathname
     * string into an abstract pathname. If the given string is the empty
     * string, then the result is the empty abstract pathname.
     *
     * @param p_pathname A pathname string
     * @throws NullPointerException If the <code>pathname</code> argument is
     *                              {@code null}
     */
    public Directory(final String p_pathname,
                 final EFileValidator... p_permissions) {
        super(p_pathname);
        this.d_verify(p_permissions);
    }

    /**
     * Creates a new {@code File} instance from a parent pathname string and a
     * child pathname string.
     * <p/>
     * <p> If <code>parent</code> is <code>null</code> then the new {@code File}
     * instance is created as if by invoking the single-argument
     * <code>File</code> constructor on the given {@code child} pathname
     * string.
     * <p/>
     * <p> Otherwise the <code>parent</code> pathname string is taken to denote
     * a directory, and the {@code child} pathname string is taken to denote
     * either a directory or a file.  If the {@code child} pathname string is
     * absolute then it is converted into a relative pathname in a
     * system-dependent way.  If {@code parent} is the empty string then the new
     * <code>File</code> instance is created by converting {@code child} into an
     * abstract pathname and resolving the result against a system-dependent
     * default directory.  Otherwise each pathname string is converted into an
     * abstract pathname and the child abstract pathname is resolved against the
     * parent.
     *
     * @param p_parent The parent pathname string
     * @param p_child  The child pathname string
     * @throws NullPointerException If <code>child</code> is {@code null}
     */
    public Directory(final String p_parent, final String p_child,
                 final EFileValidator... p_permissions) {
        super(p_parent, p_child);
        this.d_verify(p_permissions);
    }

    /**
     * Creates a new {@code File} instance from a parent abstract pathname and a
     * child pathname string.
     * <p/>
     * <p> If <code>parent</code> is <code>null</code> then the new {@code File}
     * instance is created as if by invoking the single-argument
     * <code>File</code> constructor on the given {@code child} pathname
     * string.
     * <p/>
     * <p> Otherwise the {@code parent} abstract pathname is taken to denote a
     * directory, and the <code>child</code> pathname string is taken to denote
     * either a directory or a file.  If the {@code child} pathname string is
     * absolute then it is converted into a relative pathname in a
     * system-dependent way.  If <code>parent</code> is the empty abstract
     * pathname then the new {@code File} instance is created by converting
     * {@code child} into an abstract pathname and resolving the result against
     * a system-dependent default directory.  Otherwise each pathname string is
     * converted into an abstract pathname and the child abstract pathname is
     * resolved against the parent.
     *
     * @param p_parent The parent abstract pathname
     * @param p_child  The child pathname string
     * @throws NullPointerException If <code>child</code> is {@code null}
     */
    public Directory(final java.io.File p_parent, final String p_child,
                 final EFileValidator... p_permissions) {
        super(p_parent, p_child);
        this.d_verify(p_permissions);
    }

    /**
     * Creates a new <tt>File</tt> instance by converting the given
     * <tt>file:</tt> URI into an abstract pathname.
     * <p/>
     * <p> The exact form of a <tt>file:</tt> URI is system-dependent, hence the
     * transformation performed by this constructor is also system-dependent.
     * <p/>
     * <p> For a given abstract pathname <i>f</i> it is guaranteed that
     * <p/>
     * <blockquote><tt> new File(</tt><i>&nbsp;f</i><tt>.{@link #toURI()
     * toURI}()).equals(</tt><i>&nbsp;f</i><tt>.{@link #getAbsoluteFile()
     * getAbsoluteFile}()) </tt></blockquote>
     * <p/>
     * so long as the original abstract pathname, the URI, and the new abstract
     * pathname are all created in (possibly different invocations of) the same
     * Java virtual machine.  This relationship typically does not hold,
     * however, when a <tt>file:</tt> URI that is created in a virtual machine
     * on one operating system is converted into an abstract pathname in a
     * virtual machine on a different operating system.
     *
     * @param p_uri An absolute, hierarchical URI with a scheme equal to
     *              <tt>"file"</tt>, a non-empty path component, and undefined
     *              authority, query, and fragment components
     * @throws NullPointerException     If <tt>uri</tt> is <tt>null</tt>
     * @throws IllegalArgumentException If the preconditions on the parameter do
     *                                  not hold
     * @see #toURI()
     * @see URI
     * @since 1.4
     */
    public Directory(final URI p_uri,
                 final EFileValidator... p_permissions) {
        super(p_uri);
        this.d_verify(p_permissions);
    }

    public boolean isValid() {
        return this.m_isValid;
    }

    /**
     * Tests, if the directory is usable.
     *
     * @param p_behaviours Behaviour on errors.
     * @return true, if usable.
     */
    private void d_verify(final EFileValidator[] p_permissions) {
        boolean l_autoCreate = false;
        boolean l_readable = false;
        boolean l_writable = false;
        if (p_permissions == null) {

        } else {
            for (final EFileValidator l_permission : p_permissions) {
                switch (l_permission) {
                    case AutoCreate:
                        l_autoCreate = true;
                        break;
                    case Read:
                        l_readable = true;
                        break;
                    case Write:
                        l_writable = true;
                        break;
                    case None:
                        break;
                }
            }
        }

        if (super.exists()) {
            if (super.isDirectory()) {
                if (!super.canRead() && l_readable) {
                    if (!super.setReadable(true)) {
//                        Log.inst()
//                            .error("This directory has wrong permissions: " +
//                                super.getName(), "FileManager");
                        this.m_isValid = false;
                    }
                }
                if (!super.canWrite() && l_writable) {
                    if (!super.setWritable(true)) {
//                        Log.inst()
//                            .error("This directory has wrong permissions: " +
//                                super.getName(), "FileManager");
                        this.m_isValid = false;
                    }
                }
            } else {
//                Log.inst()
//                    .error("This directory is actually a directory: " +
//                        super.getName(), "FileManager");
                this.m_isValid = false;
            }
        } else {
            if (l_autoCreate) {
//                Log.inst()
//                    .warn("This directory is not existent: " + super.getName(),
//                        "FileManager");
                try {
                    super.createNewFile();
//                    Log.inst()
//                        .warn("Directory was created: " + super.getName(),
//                            "FileManager");
                } catch (final @NotNull IOException l_e) {
//                    Log.inst()
//                        .error("This directory could not be created:" +
//                            super.getName(), "FileManager");
                    this.m_isValid = false;
                }
            } else {
//                Log.inst()
//                    .warn("This directory is not existent:" + super.getName(),
//                        "FileManager");
                this.m_isValid = false;
            }
        }
    }
    public String getDirectoryName() {
        return FilenameUtils.getBaseName(super.getName());
    }
}
