/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.util.fileUtil;

import org.apache.commons.io.FilenameUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;

public final class FileUtil {

    private FileUtil() {

    }

    /**
     * This will return the base name of a set of files. It is used to get the
     * names of all available record types.
     *
     * @param p_input Files to get the basename of.
     * @return Base name of the input1 files.
     */
    public static @NotNull String[] getBaseNames(final @NotNull File[] p_input) {
        final String[] l_names = new String[p_input.length];
        for (int l_i = 0; l_i < p_input.length; l_i++) {
            l_names[l_i] = FilenameUtils.getBaseName(p_input[l_i].getName());
        }
        return l_names;
    }
}
