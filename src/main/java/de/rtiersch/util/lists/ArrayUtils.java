/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.util.lists;

import de.rtiersch.util.lists.intern.ExtractedArray;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Locale;
import java.util.stream.IntStream;

/**
 * Array utilities. Extension to apache commons.
 */
public final class ArrayUtils
{

    /**
     * Empty String array.
     */
    public static final String[] STRINGS       = new String[0];
    /**
     * All big english letters (canonial alphabet in the roman script).
     */
    public static final char[]   BIG_LETTER    =
        {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
            'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
    /**
     * All small english letters (canonial alphabet in the roman script).
     */
    public static final char[]   SMALL_LETTERS =
        {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
            'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

    /**
     * private utility constructor.
     */
    private ArrayUtils()
    {
    }

    /**
     * Mass stringification of objects.
     *
     * @param p_array List to get the string representations of.
     * @return All the string representations of the objects.
     */
    public static String[] toString(final @NotNull Object... p_array)
    {
        final String[] l_return = new String[p_array.length];
        for (int l_i = 0; l_i < p_array.length; l_i++) {
            l_return[l_i] = p_array[l_i].toString();
        }
        return l_return;
    }

    /**
     * Creates a new Object an fills it with p_default.
     *
     * @param p_default default object that will be filled into this object.
     * @param p_length  wished length of the new object.
     * @return filled Object.
     */
    @NotNull
    public static Object[] createArrayDefault(
        @NotNull final Object p_default, final int p_length)
    {
        final Object[] l_return = new Object[p_length];
        for (int l_i = 0; l_i < l_return.length; l_i++) {
            l_return[l_i] = p_default;
        }
        return l_return;
    }

    /**
     * Return the length of the longest string of the list.
     *
     * @param p_strings List of strings
     * @return Length of the longest string.
     */
    public static int getLongestString(
        @NotNull final CharSequence[] p_strings)
    {
        int l_length = 0;
        for (final CharSequence l_string : p_strings) {
            if (l_string.length() > l_length) {
                l_length = l_string.length();
            }
        }
        return l_length;
    }

    /**
     * Return the length of the longest string of the list.
     *
     * @param p_strings List of strings
     * @return Length of the longest string.
     */
    public static int getLongestString(
        @NotNull final Iterable<CharSequence> p_strings)
    {
        int l_length = 0;
        for (final CharSequence l_string : p_strings) {
            if (l_string.length() > l_length) {
                l_length = l_string.length();
            }
        }
        return l_length;
    }

    /**
     * Transforms the matrix and switches the indices.
     *
     * @param p_array Input to transform. Will not be changed.
     * @return Transformed array with x=y' && y=x'.
     *
     * @throws NullPointerException Y has different length.
     */
    public static @NotNull Object[][] changeXYtoYX(
        final @NotNull Object[][] p_array)
    {
        final int l_xOld = p_array.length;
        final int l_yOld;
        if (l_xOld == 0) {
            l_yOld = 0;
        } else {
            l_yOld = p_array[0].length;
        }
        final Object[][] l_new = new Object[l_yOld][l_xOld];

        for (int l_x = 0; l_x < l_xOld; l_x++) {
            for (int l_y = 0; l_y < l_yOld; l_y++) {
                l_new[l_y][l_x] = p_array[l_x][l_y];
            }
        }
        return l_new;
    }

    /**
     * Checks all arrays have the same length.
     *
     * @param p_array input to validate.
     * @return true if input array is square.
     */
    public static boolean isSquare(final @NotNull Object[][] p_array)
    {
        final int l_lengthA = p_array.length;
        if (l_lengthA == 0) {
            return true;
        }
        return IntStream.range(0, l_lengthA)
                        .noneMatch(l_i -> p_array[l_i].length != l_lengthA);
    }

    /**
     * Checks if all second indiced arrays have the same length.
     *
     * @param p_array input to validate.
     * @return true if array is square.
     */
    public static boolean isRectangular(final @NotNull Object[][] p_array)
    {
        final int l_lengthA = p_array.length;
        if (l_lengthA == 0) {
            return true;
        }
        final int l_lengthB = p_array[0].length;
        return IntStream.range(1, l_lengthA)
                        .noneMatch(l_i -> p_array[l_i].length != l_lengthB);
    }

    /**
     * Concatenates Objects with a delimiter to a single String.
     *
     * @param p_concatenator Concatenation array which will be placed between
     *                       each object.
     * @param p_array        Array which will be combined with the concatenator
     *                       in between.
     * @return Concatenated String.
     */
    public static String concat(
        final @NotNull String p_concatenator, final @NotNull Object... p_array)
    {
        StringBuilder l_sb = new StringBuilder();
        l_sb.append(p_array[0]);
        for (int l_i = 1; l_i < p_array.length; l_i++) {
            l_sb.append(p_concatenator);
            l_sb.append(p_array[l_i]);
        }
        return l_sb.toString();
    }

    /**
     * Concatenates Objects with a delimiter to a single String.
     *
     * @param p_concatenator Concatenation array which will be placed between
     *                       each object.
     * @param p_array        Array which will be combined with the concatenator
     *                       in between.
     * @return Concatenated String.
     */
    public static String concat(
        final @NotNull String p_concatenator,
        final @NotNull ArrayList<Object> p_array)
    {
        if (p_array.isEmpty()) {
            return "";
        }
        StringBuilder l_sb = new StringBuilder();
        l_sb.append(p_array.get(0));
        for (int l_i = 1; l_i < p_array.size(); l_i++) {
            l_sb.append(p_concatenator);
            l_sb.append(p_array.get(l_i));
        }
        return l_sb.toString();
    }

    /**
     * Tests for null content. If there is any, return true.
     *
     * @param p_arrays object to scan.
     * @return true if found any null.
     */
    public static boolean testNulls(@Nullable final Object[]... p_arrays)
    {
        if (p_arrays == null) {
            return true;
        }
        for (final Object[] l_array : p_arrays) {
            if (ArrayUtils.testNull(l_array)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Tests for null content. If there is any, return true.
     *
     * @param p_array object to scan.
     * @return true if found any null.
     */
    public static boolean testNull(@Nullable final Object... p_array)
    {
        if (p_array == null) {
            return true;
        }
        for (final Object l_elem : p_array) {
            if (l_elem == null) {
                return true;
            }
        }
        return false;
    }

    /**
     * Tests for null content. If there is any, return true.
     *
     * @param p_arrays object to scan.
     * @return true if found any null.
     */
    public static boolean testNulls(@Nullable final Object[][]... p_arrays)
    {
        if (p_arrays == null) {
            return true;
        }
        for (final Object[][] l_array : p_arrays) {
            if (ArrayUtils.testNull(l_array)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Tests for null content. If there is any, return true.
     *
     * @param p_array object to scan.
     * @return true if found any null.
     */
    public static boolean testNull(@Nullable final Object[][] p_array)
    {
        if (p_array == null) {
            return true;
        }
        for (final Object[] l_elem : p_array) {
            if (ArrayUtils.testNull(l_elem)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Makes sure that every line has the same length.
     *
     * @param p_input   Array that should be validated and modified.
     * @param p_default Default value that will be filled in not existing
     *                  fields.
     * @return Rectified Copy of the input array.
     */
    @NotNull
    public static String[][] rectify(
        @NotNull final String[][] p_input, @NotNull final String p_default)
    {
        final int l_lengthFirst = p_input.length;

        if (l_lengthFirst <= 1) {
            return p_input;
        }

        // Find longest second.
        int l_lengthSecond = 0;
        for (String[] l_strings : p_input) {
            if (l_strings.length > l_lengthSecond) {
                l_lengthSecond = l_strings.length;
            }
        }

        // create new Array.
        final String[][] l_new = new String[l_lengthFirst][l_lengthSecond];
        for (int l_first = 0; l_first < l_lengthFirst; l_first++) {
            int l_second = 0;
            for (; l_second < p_input[l_first].length; l_second++) {
                if (p_input[l_first][l_second] == null) {
                    l_new[l_first][l_second] = p_default;
                } else {
                    l_new[l_first][l_second] = p_input[l_first][l_second];
                }
            }
            for (; l_second < l_lengthSecond; l_second++) {
                l_new[l_first][l_second] = p_default;
            }

        }
        return l_new;
    }

    /**
     * Makes sure that every line has the same length.
     *
     * @param p_input   Array that should be validated and modified.
     * @param p_default Default value that will be filled in not existing
     *                  fields.
     * @return Rectified copy of input array p_input.
     */
    @NotNull
    public static String[][] rectifyUnsafe(
        @NotNull final String[][] p_input, @Nullable final String p_default)
    {
        final int l_lengthFirst = p_input.length;

        if (l_lengthFirst <= 1) {
            return p_input;
        }

        // Find longest second.
        int l_lengthSecond = 0;
        for (String[] l_strings : p_input) {
            if (l_strings.length > l_lengthSecond) {
                l_lengthSecond = l_strings.length;
            }
        }

        // create new Array.
        final String[][] l_new = new String[l_lengthFirst][l_lengthSecond];
        for (int l_first = 0; l_first < l_lengthFirst; l_first++) {
            int l_second = 0;
            for (; l_second < p_input[l_first].length; l_second++) {
                l_new[l_first][l_second] = p_input[l_first][l_second];
            }
            for (; l_second < l_lengthSecond; l_second++) {
                l_new[l_first][l_second] = p_default;
            }

        }
        return l_new;
    }

    /**
     * Removes all the null values out of every single Array.
     *
     * @param p_input   Array to be verified.
     * @param p_default default value used for NULL fields..
     */
    public static void denullify(
        @NotNull final String[][] p_input, @NotNull final String p_default)
    {
        for (int l_first = 0; l_first < p_input.length; l_first++) {
            String[] l_strings = p_input[l_first];

            if (l_strings == null) {
                p_input[l_first] = STRINGS;
            }

            for (int l_second = 0; l_second < l_strings.length; l_second++) {
                if (l_strings[l_second] == null) {
                    l_strings[l_second] = p_default;
                }
            }
        }

    }

    /**
     * Removes element number p_i out of p_elements from the first index
     * (array[this][]).
     *
     * @param p_elements Array where p_i should be removed from.
     * @param p_i        index to remove.
     * @param <E>        Type of the Array.
     * @return Extracted Array containing the array itself and the removed
     * content.
     */
    public static <E> ExtractedArray<E> cutFirstIndex(
        @NotNull final E[][] p_elements, final int p_i)
    {
        if (p_elements.length == 0) {
            E[] l_new1 = (E[]) new Object[0];
            E[][] l_new2 = (E[][]) new Object[0][0];
            return new ExtractedArray<>(l_new1, l_new2);
        }
        final E[] l_cut = p_elements[p_i];
        final E[][] l_rest = (E[][]) new Object[p_elements.length - 1][0];
        for (int l_i = 0; l_i < p_elements.length; l_i++) {
            if (l_i == p_i) {
                continue;
            }
            if (l_i > p_i) {
                l_rest[l_i - 1] = p_elements[l_i];
                continue;
            }
            l_rest[l_i] = p_elements[l_i];
        }
        return new ExtractedArray<>(l_cut, l_rest);
    }

    /**
     * Removes element number p_j out of p_elements from the second index
     * (array[][this]).
     *
     * @param p_elements Array where p_j should be removed from.
     * @param p_j        index to remove.
     * @param <E>        Type of the Array.
     * @return Extracted Array containing the array itself and the removed
     * content.
     */
    public static <E> ExtractedArray<E> cutSecondIndex(
        @NotNull final E[][] p_elements, final int p_j)
    {
        final E[] l_cut = (E[]) new Object[p_elements.length];
        final E[][] l_rest = (E[][]) new Object[p_elements.length][0];
        for (int l_i = 0; l_i < p_elements.length; l_i++) {
            l_rest[l_i] = (E[]) new Object[p_elements[0].length - 1];
            for (int l_j = 0; l_j < p_elements[0].length; l_j++) {
                if (p_j == l_j) {
                    l_cut[l_i] = p_elements[l_i][l_j];
                    continue;
                }
                if (l_j > p_j) {
                    l_rest[l_i][l_j - 1] = p_elements[l_i][l_j];
                    continue;
                }
                l_rest[l_i][l_j] = p_elements[l_i][l_j];
            }
        }
        return new ExtractedArray<>(l_cut, l_rest);
    }

    /**
     * Creates a Array full of numbers.
     *
     * @param p_locale   How should the number be formatted?
     * @param p_start    first number
     * @param p_step     step to increase p_start each element.
     * @param p_subComma how many numbers after the comma should be there.
     * @param p_length   Length of the new String array.
     * @return New Array filled with numbers.
     */
    @NotNull
    public static String[] createNumberedArray(
        @NotNull final Locale p_locale,
        final double p_start,
        final double p_step,
        final int p_subComma,
        final int p_length)
    {
        double l_number = p_start;
        final String l_dm = "%." + p_subComma + "f";
        final String[] l_return = new String[p_length];
        for (int l_length = 0; l_length < p_length; l_length++) {
            final String l_string = String.format(p_locale, l_dm, l_number);
            l_return[l_length] = l_string;
            l_number += p_step;
        }
        return l_return;
    }

    /**
     * Creates an array containing only big letters, increasing each time.
     *
     * @param p_length length of the Array.
     * @return increasing big letters string array.
     */
    public static String[] createAlphaNumericArray(final int p_length)
    {
        return createSetArray(BIG_LETTER, p_length);
    }

    /**
     * Creates an array where every element is one bigger than the on before.
     * This is not useful for math-safe stuff. If the input array is [0-9]
     * there will be array elements "0" and ten later will be "00". It is
     * still usefull for a set known from Escel ("A", "B", ..., "AA", "AB").
     *
     * @param p_set    size sorted dictionary. the first is the lowest.
     * @param p_length length of the new array.
     * @return New Array with unique string identifiers.
     */
    public static String[] createSetArray(
        @NotNull final char[] p_set, final int p_length)
    {
        final int l_lengthSet = p_set.length;
        final String[] l_return = new String[p_length];
        int l_i = 0;

        final ArrayList<Integer> l_indices = new ArrayList<>(2);
        l_indices.add(0);
        int l_lastIndex = 0;
        for (; l_i < p_length; l_i++) {

            l_return[l_i] = createStringFromIndexSet(l_indices, p_set);

            // Increasing value for next round
            int l_currentIt = l_lastIndex;
            boolean l_finish = false;
            // if last index of set is reached on current position
            while (l_indices.get(l_currentIt) == l_lengthSet - 1) {
                // reset last index and proceed with previous one.
                l_indices.set(l_currentIt, 0);
                l_currentIt -= 1;
                // Catch All at maximum.
                if (l_currentIt < 0) {
                    l_indices.add(0, 0);
                    l_lastIndex++;
                    // Reset all following indices to zero.
                    for (int l_k = 0; l_k < l_indices.size(); l_k++) {
                        l_indices.set(l_k, 0);
                    }
                    l_finish = true;
                    break;
                }
            }
            if (!l_finish) {
                l_indices.set(l_currentIt, l_indices.get(l_currentIt) + 1);
            }
        }
        return l_return;
    }

    /**
     * Creates a string out of the mapped chars p_set with the index list
     * p_indices.
     *
     * @param p_indices Which indices of p_set will be combined to a string.
     * @param p_set     dictionary.
     * @return new String of p_set's.
     */
    public static String createStringFromIndexSet(
        final @NotNull ArrayList<Integer> p_indices,
        final @NotNull char[] p_set)
    {
        final char[] l_return = new char[p_indices.size()];
        for (int l_i = 0; l_i < p_indices.size(); l_i++) {
            l_return[l_i] = p_set[p_indices.get(l_i)];
        }
        return new String(l_return);
    }
}
