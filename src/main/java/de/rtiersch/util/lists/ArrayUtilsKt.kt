/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.util.lists

import de.rtiersch.exception.ArrayNotRectangularException
import de.rtiersch.util.annotation.API
import de.rtiersch.util.lists.intern.ExtractedArray
import java.util.*
import java.util.stream.IntStream
import kotlin.collections.ArrayList

/**
 * Array utilities. Extension to apache commons.
 */
@API
object ArrayUtilsKt {

    /**
     * Empty String array.
     */
    @API
    val STRINGS = emptyArray<String>()
    /**
     * All big english letters (canonical alphabet in the roman script).
     */
    @API
    val BIG_LETTER = charArrayOf('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
        'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
        'X', 'Y', 'Z')
    /**
     * All small english letters (canonical alphabet in the roman script).
     */
    @API
    val SMALL_LETTERS = charArrayOf('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
        'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
        'x', 'y', 'z')

    /**
     * Mass stringification of objects.
     *
     * @param p_array List to get the string representations of.
     * @return All the string representations of the objects.
     */
    @API
    @Suppress("UNCHECKED_CAST")
    fun toString(p_array: Array<Any>): Array<String> {
        val l_return = arrayOfNulls<String>(p_array.size)
        for (l_i in p_array.indices) {
            l_return[l_i] = p_array[l_i].toString()
        }
        return l_return as Array<String>
    }

    /**
     * Creates a new Object an fills it with p_default.
     *
     * @param p_default default object that will be filled into this object.
     * @param p_length  wished length of the new object.
     * @return filled Object.
     */
    @API
    @Suppress("UNCHECKED_CAST")
    fun createArrayDefault(p_default: Any,
                           p_length: Int): Array<Any> {
        val l_return = arrayOfNulls<Any>(p_length)
        for (l_i in l_return.indices) {
            l_return[l_i] = p_default
        }
        return l_return as Array<Any>
    }

    /**
     * Return the length of the longest string of the list.
     *
     * @param p_strings List of strings
     * @return Length of the longest string.
     */
    @API
    fun getLongestString(p_strings: Array<CharSequence>): Int {
        var l_length = 0
        p_strings.asSequence().filter { it.length > l_length }.forEach { l_length = it.length }
        return l_length
    }

    /**
     * Return the length of the longest string of the list.
     *
     * @param p_strings List of strings
     * @return Length of the longest string.
     */
    @API
    fun getLongestString(p_strings: Iterable<CharSequence>): Int {
        var l_length = 0
        p_strings.asSequence().filter { it.length > l_length }.forEach { l_length = it.length }
        return l_length
    }

    /**
     * Transforms the matrix and switches the indices.
     *
     * @param p_array Input to transform. Will not be changed.
     * @return Transformed array with x=y' && y=x'.
     * @throws ArrayNotRectangularException Y has different length.
     */
    @API
    @Suppress("UNCHECKED_CAST")
    fun changeXYtoYX(p_array: Array<Array<Any>>): Array<Array<Any>> {
        if (!ArrayUtilsKt.isRectangular(p_array)) {
            throw ArrayNotRectangularException(
                "Input array cannot be transformed due" + " to unequal length of the individual arrays.")
        }
        val l_xOld = p_array.size
        val l_yOld: Int
        if (l_xOld == 0) {
            l_yOld = 0
        } else {
            l_yOld = p_array[0].size
        }
        val l_new = Array(l_yOld) {
            arrayOfNulls<Any>(l_xOld)
        }

        for (l_x in 0 until l_xOld) {
            for (l_y in 0 until l_yOld) {
                l_new[l_y][l_x] = p_array[l_x][l_y]
            }
        }
        return l_new as Array<Array<Any>>
    }

    /**
     * Checks all arrays have the same length.
     *
     * @param p_array input to validate.
     * @return true if input array is square.
     */
    @API
    fun isSquare(p_array: Array<Array<Any>>): Boolean {
        val l_lengthA = p_array.size
        if (l_lengthA == 0) {
            return true
        }
        return IntStream.range(0, l_lengthA).noneMatch(
            { l_i -> p_array[l_i].size != l_lengthA })
    }

    /**
     * Checks if all second indexed arrays have the same length.
     *
     * @param p_array input to validate.
     * @return true if array is square.
     */
    @API
    fun isRectangular(p_array: Array<Array<Any>>): Boolean {
        val l_lengthA = p_array.size
        if (l_lengthA == 0) {
            return true
        }
        val l_lengthB = p_array[0].size
        return IntStream.range(1, l_lengthA).noneMatch(
            { l_i -> p_array[l_i].size != l_lengthB })
    }

    /**
     * Concatenates Objects with a delimiter to a single String.
     *
     * @param p_concatenator Concatenation array which will be placed between
     *                       each object.
     * @param p_array        Array which will be combined with the concatenator
     *                       in between.
     * @return Concatenated String.
     */
    @API
    fun concat(p_concatenator: String,
               vararg p_array: Any): String {
        val l_sb = StringBuilder()
        l_sb.append(p_array[0])
        for (l_i in 1 until p_array.size) {
            l_sb.append(p_concatenator)
            l_sb.append(p_array[l_i])
        }
        return l_sb.toString()
    }

    /**
     * Concatenates Objects with a delimiter to a single String.
     *
     * @param p_concatenator Concatenation array which will be placed between
     *                       each object.
     * @param p_array        Array which will be combined with the concatenator
     *                       in between.
     * @return Concatenated String.
     */
    @API
    fun concat(p_concatenator: String,
               p_array: ArrayList<Any>): String {
        if (p_array.isEmpty()) {
            return ""
        }
        val l_sb = StringBuilder()
        l_sb.append(p_array[0])
        for (l_i in 1 until p_array.size) {
            l_sb.append(p_concatenator)
            l_sb.append(p_array[l_i])
        }
        return l_sb.toString()
    }

    /**
     * Tests for null content. If there is any, return true.
     *
     * @param p_arrays object to scan.
     * @return true if found any null.
     */
    @API
    fun testNulls(vararg p_arrays: Array<Any?>?): Boolean {
        for (l_array in p_arrays) {
            if (l_array == null) {
                return true
            }
            if (ArrayUtilsKt.testNull(l_array)) {
                //if (ArrayUtilsKt.testNull(*l_array)) {
                return true
            }
        }
        return false
    }

    /**
     * Tests for null content. If there is any, return true.
     *
     * @param p_array object to scan.
     * @return true if found any null.
     */
    @API
    fun testNull(p_array: Array<Any?>): Boolean {
        //fun testNull(vararg p_array: Any?): Boolean {
        return p_array.contains(null)
    }

    /**
     * Tests for null content. If there is any, return true.
     *
     * @param p_arrays object to scan.
     * @return true if found any null.
     */
    @API
    fun testNulls(vararg p_arrays: Array<Array<Any?>?>?): Boolean {
        return p_arrays.any { ArrayUtilsKt.testNull(it) }
    }

    /**
     * Tests for null content. If there is any, return true.
     *
     * @param p_array object to scan.
     * @return true if found any null.
     */
    @API
    fun testNull(p_array: Array<Array<Any?>?>?): Boolean {
        if (p_array == null) {
            return true
        }
        for (l_elem in p_array) {
            if (l_elem == null) {
                return true
            }
            if (ArrayUtilsKt.testNull(l_elem)) {
                //if (ArrayUtilsKt.testNull(*l_elem)) {
                return true
            }
        }
        return false
    }

    /**
     * Makes sure that every line has the same length.
     *
     * @param p_input   Array that should be validated and modified.
     * @param p_default Default value that will be filled in not existing
     *                  fields.
     * @return Rectified Copy of the input array.
     */
    @API
    @Suppress("UNCHECKED_CAST")
    fun rectify(p_input: Array<Array<String?>>,
                p_default: String): Array<Array<String>> {
        val l_lengthFirst = p_input.size

        if (l_lengthFirst == 0) {
            return p_input as Array<Array<String>>
        }

        // Find longest second.
        var l_lengthSecond = 0
        p_input.asSequence().filter { it.size > l_lengthSecond }.forEach { l_lengthSecond = it.size }

        // create new Array.
        val l_new = Array(l_lengthFirst) {
            arrayOfNulls<String>(l_lengthSecond)
        }
        for (l_first in 0 until l_lengthFirst) {
            var l_second = 0
            while (l_second < p_input[l_first].size) {
                if (p_input[l_first][l_second] == null) {
                    l_new[l_first][l_second] = p_default
                } else {
                    l_new[l_first][l_second] = p_input[l_first][l_second]
                }
                l_second++
            }
            while (l_second < l_lengthSecond) {
                l_new[l_first][l_second] = p_default
                l_second++
            }

        }
        return l_new as Array<Array<String>>
    }

    /**
     * Makes sure that every line has the same length.
     *
     * @param p_input   Array that should be validated and modified.
     * @param p_default Default value that will be filled in not existing
     *                  fields.
     * @return Rectified copy of input array p_input.
     */
    @API
    fun rectifyUnsafe(p_input: Array<Array<String?>>,
                      p_default: String?): Array<Array<String?>> {
        val l_lengthFirst = p_input.size

        if (l_lengthFirst <= 1) {
            return p_input
        }

        // Find longest second.
        var l_lengthSecond = 0
        p_input.asSequence().filter { it.size > l_lengthSecond }.forEach { l_lengthSecond = it.size }

        // create new Array.
        val l_new = Array(l_lengthFirst) {
            arrayOfNulls<String>(l_lengthSecond)
        }
        for (l_first in 0 until l_lengthFirst) {
            var l_second = 0
            while (l_second < p_input[l_first].size) {
                l_new[l_first][l_second] = p_input[l_first][l_second]
                l_second++
            }
            while (l_second < l_lengthSecond) {
                l_new[l_first][l_second] = p_default
                l_second++
            }

        }
        return l_new
    }

    /**
     * Removes all the null values out of every single Array.
     *
     * @param p_input   Array to be verified.
     * @param p_default default value used for NULL fields..
     */
    @API
    @Suppress("UNCHECKED_CAST")
    fun deNullify(p_input: Array<Array<String?>?>,
                  p_default: String): Array<Array<String>> {
        for (l_index in p_input.indices) {
            val l_strings = p_input[l_index]

            if (l_strings == null) {
                p_input[l_index] = STRINGS as Array<String?>
            } else {
                l_strings.indices.asSequence().filter { l_strings[it] == null }.forEach { l_strings[it] = p_default }
            }
        }
        return p_input as Array<Array<String>>
    }

    /**
     * Removes element number p_i out of p_elements from the first index
     * (array[this one][]).
     *
     * @param p_elements Array where p_i should be removed from.
     * @param p_i        index to remove.
     * @param <E>        Type of the Array.
     * @return Extracted Array containing the array itself and the removed content.
     */
    @API
    @Suppress("UNCHECKED_CAST")
    fun <E> cutFirstIndex(p_elements: Array<Array<E>>,
                          p_i: Int): ExtractedArray<E> {
        if (p_elements.isEmpty()) {
            val l_new1: Array<E> = emptyArray<Any>() as Array<E>
            val l_new2: Array<Array<E>> = emptyArray()
            return ExtractedArray(l_new1, l_new2)
        }
        val l_cut = p_elements[p_i]
        val l_rest = Array(p_elements.size - 1) {
            emptyArray<Any>()
        } as Array<Array<E>>
        for (l_i in p_elements.indices) {
            if (l_i == p_i) {
                continue
            }
            if (l_i > p_i) {
                l_rest[l_i - 1] = p_elements[l_i]
                continue
            }
            l_rest[l_i] = p_elements[l_i]
        }
        return ExtractedArray(l_cut, l_rest)
    }

    /**
     * Removes element number p_j out of p_elements from the second index
     * (array[][this one]).
     *
     * @param p_elements Array where p_j should be removed from.
     * @param p_j        index to remove.
     * @param <E>        Type of the Array.
     * @return Extracted Array containing the array itself and the removed content.
     */
    @API
    @Suppress("UNCHECKED_CAST")
    fun <E> cutSecondIndex(p_elements: Array<Array<E>>,
                           p_j: Int): ExtractedArray<E> {
        if (p_elements.isEmpty()) {
            val l_cut = emptyArray<Any>() as Array<E>
            val l_rest = Array(0) { emptyArray<Any>() } as Array<Array<E>>
            return ExtractedArray(l_cut, l_rest)
        }
        if (p_elements.all { it -> it.isEmpty() }) {
            val l_cut = emptyArray<Any>() as Array<E>
            val l_rest = Array(0) { emptyArray<Any>() } as Array<Array<E>>
            return ExtractedArray(l_cut, l_rest)
        }
        if (p_elements.any { it -> it.size <= p_j }) {
            throw(ArrayIndexOutOfBoundsException())
        }
        val l_cut = arrayOfNulls<Any>(p_elements.size) as Array<E>
        val l_rest = Array(p_elements.size) {
            emptyArray<Any>()
        } as Array<Array<E>>
        for (l_i in p_elements.indices) {
            if (p_elements[0].isEmpty()) {
                l_rest[l_i] = emptyArray<Any>() as Array<E>
            } else {
                l_rest[l_i] = arrayOfNulls<Any>(
                    p_elements[0].size - 1) as Array<E>
            }
            val l_restInt = arrayOfNulls<Any>(
                p_elements[l_i].size - 1) as Array<E>
            for (l_j in 0 until p_elements[l_i].size) {
                if (p_j == l_j) {
                    l_cut[l_i] = p_elements[l_i][l_j]
                    continue
                }
                if (l_j > p_j) {
                    l_restInt[l_j - 1] = p_elements[l_i][l_j]
                    continue
                }
                l_restInt[l_j] = p_elements[l_i][l_j]
            }
            l_rest[l_i] = l_restInt
        }
        return ExtractedArray(l_cut, l_rest)
    }

    /**
     * Creates a Array full of numbers.
     *
     * @param p_locale   How should the number be formatted?
     * @param p_start    first number
     * @param p_step     step to increase p_start each element.
     * @param p_subComma how many numbers after the comma should be there.
     * @param p_length   Length of the new String array.
     * @return New Array filled with numbers.
     */
    @API
    @Suppress("UNCHECKED_CAST")
    fun createNumberedArray(p_locale: Locale,
                            p_start: Double,
                            p_step: Double,
                            p_subComma: Int,
                            p_length: Int): Array<String> {
        var l_number = p_start
        val l_dm = "%." + p_subComma + "f"
        val l_return = arrayOfNulls<String>(p_length)
        for (i in 0 until p_length) {
            val pow = Math.pow(10.0, p_subComma * 1.0)
            val number = Math.floor(l_number * pow) / pow
            val l_string = String.format(p_locale, l_dm, number)
            l_return[i] = l_string
            l_number += p_step
        }
        return l_return as Array<String>
    }

    /**
     * Extends a previously generated numbered array by length's elements.
     * Does not calculate rounding errors.
     * @param last The last element of the generated last.
     * @param penultimate The element before the last of the generated list.
     * @param length The length of the generated list.
     */
    @API
    fun createNextNumberedArray(penultimate: String,
                                last: String,
                                length: Int): Array<String> {
        val subComma: Int
        val step: Double
        val lastNumber = last.replace(',','.').toDouble()
        val locale: Locale

        if (last.contains('.')) {
            locale = Locale.ENGLISH
            subComma = last.substringAfter('.').length
        } else if (last.contains(',')) {
            locale = Locale.GERMAN
            subComma = last.substringAfter(',').length
        } else {
            locale = Locale.GERMANY
            subComma = 0
        }
        step = lastNumber - penultimate.replace(',','.').toDouble()
        val number = lastNumber + step
        return createNumberedArray(locale, number, step, subComma, length)
    }

    /**
     * Creates an array containing only big letters, increasing each time.
     *
     * @param p_length length of the Array.
     * @return increasing big letters string array.
     */
    @API
    fun createAlphaNumericArray(p_length: Int): Array<String> {
        return createSetArray(BIG_LETTER, p_length)
    }

    /**
     * Creates an array where every element is one bigger than the on before.
     * This is not useful for math-safe stuff. If the input array is [0-9]
     * there will be array elements "0" and ten later will be "00". It is
     * still useful for a set known from Excel ("A", "B", ..., "AA", "AB").
     *
     * @param p_set    size sorted dictionary. the first is the lowest.
     * @param p_length length of the new array.
     * @param neutralElement Is the first element of the set a neutral
     * element? Do not define this as true if the dicitionary only has one
     * element.
     * @param startIndexSet The index set used to start the set array.
     * @param incrementBeforeFirstUse Will ignore the first step.
     * @return New Array with unique string identifiers.
     */
    @API
    @Suppress("UNCHECKED_CAST")
    fun createSetArray(p_set: CharArray,
                       p_length: Int,
                       neutralElement: Boolean = false,
                       startIndexSet: ArrayList<Int> = START_AT_ZERO(),
                       incrementBeforeFirstUse: Boolean = false): Array<String> {
        val l_lengthSet = p_set.size
        val l_return = arrayOfNulls<String>(p_length)
        var i = if (incrementBeforeFirstUse) -1 else 0
        val newFirstElement = if (neutralElement) 1 else 0
        val indices: ArrayList<Int> = startIndexSet
        var lastIndex = indices.lastIndex
        while (i < p_length) {
            if (i >= 0) l_return[i] = createStringFromIndexSet(indices, p_set)

            // Increasing value for next round
            var currentIt = lastIndex
            var finnish = false
            // if last index of set is reached on current position
            // Current element reached the last avaiable element.
            while (indices[currentIt] == l_lengthSet - 1) {
                // reset last index and proceed with previous one.
                indices[currentIt] = 0
                currentIt -= 1
                // Catch All at maximum.
                if (currentIt < 0) {
                    indices.add(0, newFirstElement)
                    lastIndex = indices.lastIndex
                    // Reset all following indices to zero.
                    for (l_k in 1 until indices.size) {
                        indices[l_k] = 0
                    }
                    finnish = true
                    break
                }
            }
            if (!finnish) {
                indices[currentIt] = indices[currentIt] + 1
            }
            i++
        }
        return l_return as Array<String>
    }

    /**
     * Creates a string out of the mapped chars p_set with the index list
     * p_indices.
     *
     * @param p_indices Which indices of p_set will be combined to a string.
     * @param p_set     dictionary.
     * @return new String of p_set's.
     */
    @API
    fun createStringFromIndexSet(p_indices: ArrayList<Int>,
                                 p_set: CharArray): String {
        val l_return = CharArray(p_indices.size)
        for (l_i in 0 until p_indices.size) {
            l_return[l_i] = p_set[p_indices[l_i]]
        }
        return String(l_return)
    }

    /**
     * Creates a indexSet from a string which can be used to generate new
     * following incrementing strings.
     * @param content The String that should be decoded.
     * @param set The set that was used to generate that string.
     * @return the decoded string usind the set dictionary.
     */
    @API
    fun createIndexSetFromString(content: String,
                                 set: CharArray): ArrayList<Int> {
        val length = content.length
        val toReturn: ArrayList<Int>
        toReturn = ArrayList(length)
        (0..content.lastIndex).mapTo(toReturn) { set.indexOf(content[it]) }
        return toReturn
    }
}

class START_AT_ZERO : ArrayList<Int>() {
    init {
        this.add(0)
    }
}

class START_AT_ONE : ArrayList<Int>() {
    init {
        this.add(1)
    }
}
