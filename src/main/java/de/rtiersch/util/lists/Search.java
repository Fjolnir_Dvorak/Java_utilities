/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.util.lists;

import org.jetbrains.annotations.NotNull;

/**
 * Search functions.
 */
public final class Search
{

    /**
     * Private Utility Class constructor.
     */
    private Search()
    {
    }

    /**
     * This method will check if the values of input1 are in p_dict.
     *
     * @param p_input input to map on p_dict.
     * @param p_dict  Dictionary.
     * @return true, if all values of input are in p_dict.
     */
    public static boolean contains(
        final @NotNull String[] p_input, final @NotNull String[] p_dict)
    {
        final int l_length = p_input.length;
        int l_containing = 0;
        for (final String l_element : p_dict) {
            for (final String l_compare : p_input) {
                if (l_element.equals(l_compare)) {
                    l_containing++;
                    if (l_length == l_containing) {
                        return true;
                    }
                    break;
                }
            }
        }
        return false;
    }

    /**
     * This method will check if the values of p_input are in p_dict.
     *
     * @param p_input input to map on p_dict.
     * @param p_dict  Dictionary.
     * @return true, if all values of input are in p_dict.
     */
    public static boolean contains(
        final @NotNull String p_input, final @NotNull String[] p_dict)
    {
        for (final String l_element : p_dict) {
            if (l_element.equals(p_input)) {
                return true;
            }
        }
        return false;
    }
}
