/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.util.lists;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

/**
 * String utilities which are not present in apache.commons.
 */
public final class StringUtils
{

    /**
     * Private utility class constructor.
     */
    private StringUtils()
    {

    }

    /**
     * Accepts a Array of strings and concartenates them without any
     * delimiter between them.
     *
     * @param p_input String to concat together.
     * @return concartenated string.
     */
    public static @NotNull String toString(final @NotNull String[] p_input)
    {
        int l_bufferSize = 0;
        for (int l_i = 0; l_i < p_input.length; l_i++) {
            // Please do not remove. IntelliJ is sying wrongly that this is
            // always false. Sadly it can be true.
            if (p_input[l_i] == null) {
                continue;
            }
            l_bufferSize += p_input[l_i].length();
        }
        final StringBuilder l_buffer = new StringBuilder(l_bufferSize);
        for (final String l_s : p_input) {
            l_buffer.append(l_s);
        }
        return l_buffer.toString();
    }

    /**
     * Controls, whether the char p_source is inside the dictionary.
     *
     * @param p_source char to validate.
     * @param p_dict   dictionary for comparison with p_source.
     * @return true if p_source is in p_dict.
     */
    public static boolean isOfChar(
        final char p_source, final @NotNull CharSequence p_dict)
    {
        final int l_length = p_dict.length();
        for (int l_i = 0; l_i < l_length; l_i++) {
            if (p_dict.charAt(l_i) == p_source) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the index of the first appearance of p_pattern in p_source. If
     * not found, -1 will be returned.
     *
     * @param p_source  String to be searched through.
     * @param p_pattern char that will be searched in p_source.
     * @return index of first appearance. -1 if not found.
     */
    public static int getFirstMatch(
        final @NotNull String p_source, final char p_pattern)
    {
        final int l_length = p_source.length();
        for (int l_i = 0; l_i < l_length; l_i++) {
            if (p_source.charAt(l_i) == p_pattern) {
                return l_i;
            }
        }
        return -1;
    }

    /**
     * Checks if p_string is insdie of p_list.
     *
     * @param p_string String which could be in p_list.
     * @param p_list   Dictionary.
     * @return true id p_list contains p_string.
     */
    public static boolean isInside(
        @NotNull final String p_string, @NotNull final String[] p_list)
    {
        for (final String l_s : p_list) {
            if (l_s.equals(p_string)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Concatenates all Strings together.
     *
     * @param p_delimiter Delimiter for concatenation.
     * @param p_values    Values to concatenate.
     * @return All values glued together.
     */
    public static String concat(
        @NotNull final String p_delimiter, @NotNull final String... p_values)
    {
        return ArrayUtils.concat(p_delimiter, p_values);
    }

    /**
     * Concatenates all Strings together.
     *
     * @param p_delimiter Delimiter for concatenation.
     * @param p_values    Values to concatenate.
     * @return All values glued together.
     */
    public static String concat(
        @NotNull final String p_delimiter,
        @NotNull final ArrayList<String> p_values)
    {
        return ArrayUtils.concat(p_delimiter, p_values);
    }
}
