/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.util.tuple;

/**
 * A simple pair with no access-security level.
 *
 * @param <A> First Type
 * @param <B> Second Type
 */
public class Pair<A, B>
{
    /**
     * First element.
     */
    public A m_first;
    /**
     * Second element.
     */
    public B m_second;

    /**
     * Creates a new Pair with up to two different types.
     *
     * @param p_first  First element with type A.
     * @param p_second Second element with type B.
     */
    public Pair(final A p_first, final B p_second)
    {
        this.m_first = p_first;
        this.m_second = p_second;
    }
}
