/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.util.tuple;

import org.jetbrains.annotations.NotNull;

/**
 * Write Protected Position Container to save and handle table-bounds.
 */
public class TablePosition
{

    /**
     * Posiztion in row.
     */
    private final int m_row;
    /**
     * Position in column.
     */
    private final int m_column;

    /**
     * Creates a read only container to save positinal Informations.
     *
     * @param p_row    Posiztion in row.
     * @param p_column Position in column.
     */
    public TablePosition(final int p_row, final int p_column)
    {
        this.m_row = p_row;
        this.m_column = p_column;
    }

    /**
     * Returns the position index Column.
     *
     * @return Position in column.
     */
    public int getColumn()
    {
        return this.m_column;
    }

    /**
     * Checks if this Position is in the given bounds of p_first -> p_second.
     * Row and Column form p_first have to be smaller than those from p_second.
     *
     * @param p_first  Lower bound.
     * @param p_second Higher bound.
     * @return true if this is inside of p_first and p_second.
     */
    public boolean isInBounds(
        @NotNull final TablePosition p_first,
        @NotNull final TablePosition p_second)
    {
        if ((this.m_row >= p_first.m_row && this.m_row <= p_second.getRow())
            || (this.m_row <= p_first.m_row && this.m_row >= p_second.m_row))
        {
            if ((this.m_column >= p_first.m_column
                && this.m_column <= p_second.m_column) || (
                this.m_column <= p_first.m_column
                    && this.m_column >= p_second.m_column))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the position index Row.
     *
     * @return Position in row.
     */
    public int getRow()
    {
        return this.m_row;
    }
}
