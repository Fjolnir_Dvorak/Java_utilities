/*
 * Copyright (c) 2017 by Raphael.
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 */

package de.rtiersch.util.lists

import de.rtiersch.exception.ArrayNotRectangularException
import de.rtiersch.util.lists.intern.ExtractedArray

class ArrayUtilsKtTest extends spock.lang.Specification {

    // Testing createAttayDefault
    def "Creating a notnull array with a default element"() {
        given:
        String defaultElement = "asdf"
        String[] goal = ["asdf", "asdf", "asdf", "asdf"]
        int length = 4
        when:
        String[] generated = (String[]) ArrayUtilsKt.INSTANCE
            .createArrayDefault(defaultElement, length)
        then:
        Arrays.equals(goal, generated)
    }

    // Testing getLongestString
    def "Finding the longest String in an array"() {
        expect:
        ArrayUtilsKt.INSTANCE.getLongestString(elements) == longest
        where:
        longest | elements
        3       | ["a", "ab", "abc"]
        3       | ["a", "ab", "abc", "ab"]
        2       | ["ab", "a", "a", "ab"]
        0       | ["", "", ""]
    }

    // Testing changeXYtoYX
    def "Switching the indices of a square matrix"() {
        given:
        Integer[][] matrix = [[1, 2],
                              [3, 4]]
        Integer[][] goal = [[1, 3],
                            [2, 4]]
        when:
        Integer[][] transformed = ArrayUtilsKt.INSTANCE.changeXYtoYX(matrix) as
            Integer[][]
        then:
        Arrays.deepEquals(goal, transformed)
    }

    def "Switching the indices of a non square matrix"() {
        given:
        Integer[][] matrix = [[1, 2, 3],
                              [4, 5, 6]]
        Integer[][] goal = [[1, 4],
                            [2, 5],
                            [3, 6]]
        when:
        Integer[][] transformed = ArrayUtilsKt.INSTANCE.changeXYtoYX(matrix) as
            Integer[][]
        then:
        Arrays.deepEquals(goal, transformed)
    }

    def "Switching the indices of a matrix where the vectors have different length"() {
        given:
        Integer[][] matrix = [[1, 2],
                              [3]]
        when:
        Integer[][] transformed = ArrayUtilsKt.INSTANCE.changeXYtoYX(matrix) as
            Integer[][]
        then:
        thrown(ArrayNotRectangularException)
    }

    def "Switching the indices of a matrix containing some nulls"() {
        given:
        Integer[][] matrix = [[1, null],
                              [3, 4]]
        Integer[][] goal = [[1, 3],
                            [null, 4]]
        when:
        Integer[][] transformed = ArrayUtilsKt.INSTANCE.changeXYtoYX(matrix) as
            Integer[][]
        then:
        Arrays.deepEquals(goal, transformed)
    }

    def "Switching the indices of a matrix, where the matrix is empty"() {
        given:
        Integer[][] matrix = [[]]
        Integer[][] goal = [[]]
        Integer[] wrongGoal = []
        when:
        Integer[][] transformed = ArrayUtilsKt.INSTANCE.changeXYtoYX(matrix) as
            Integer[][]
        then:
        // Arrays.deepEquals(goal, transformed) God damn the dynamic type
        // casting
        Arrays.deepEquals(transformed, wrongGoal)
    }

    // Testing isSquare
    def "A square should be a square"() {
        given:
        Integer[][] square = [[1, 2],
                              [3, 4]]
        expect:
        ArrayUtilsKt.INSTANCE.isSquare(square)
    }

    def "A a 3x2 matrix should not be a square"() {
        given:
        Integer[][] square = [[1, 2],
                              [3, 4],
                              [5, 6]]
        expect:
        !ArrayUtilsKt.INSTANCE.isSquare(square)
    }

    def "A unequal matrix should not be a square"() {
        given:
        Integer[][] square = [[1, 2],
                              [3, 4, 5]]
        expect:
        !ArrayUtilsKt.INSTANCE.isSquare(square)
    }

    // Testing isRectangular
    def "A a 3x2 matrix should be rectangular"() {
        given:
        Integer[][] square = [[1, 2],
                              [3, 4],
                              [5, 6]]
        expect:
        ArrayUtilsKt.INSTANCE.isRectangular(square)
    }

    def "A unequal matrix should not be rectangular"() {
        given:
        Integer[][] square = [[1, 2],
                              [3, 4, 5]]
        expect:
        !ArrayUtilsKt.INSTANCE.isRectangular(square)
    }

    // Testing Concat
    def "Concatenating an Array of Strings"() {
        given:
        String[] array = ["Hallo,", "ich", "bin", "ein", "Test."]
        String goal = "Hallo, ich bin ein Test."
        when:
        String concat = ArrayUtilsKt.INSTANCE.concat(" ", array)
        then:
        goal == concat
    }

    // Testing restNull
    def "There are some nulls in this array"() {
        given:
        Integer[] array = [1, 2, null, 4, 5]
        expect:
        ArrayUtilsKt.INSTANCE.testNull(array)
    }

    def "There are no nulls in this array"() {
        given:
        Integer[] array = [1, 2, 3, 4, 5]
        expect:
        !ArrayUtilsKt.INSTANCE.testNull(array)
    }

    // Testing rectify
    def "There is nothing to rectify"() {
        given:
        String[][] matrix = [["a", "b"],
                             ["c", "d"]]
        String defValue = "z"
        when:
        String[][] result = ArrayUtilsKt.INSTANCE.rectify(matrix, defValue)
        then:
        Arrays.deepEquals(matrix, result)
    }

    def "There is nothing to rectify, but there are null values"() {
        given:
        String[][] matrix = [["a", "b"],
                             [null, "d"]]
        String[][] goal = [["a", "b"],
                           ["z", "d"]]
        String defValue = "z"
        when:
        String[][] result = ArrayUtilsKt.INSTANCE.rectify(matrix, defValue)
        then:
        Arrays.deepEquals(goal, result)
    }

    def "There is something to rectify"() {
        given:
        String[][] matrix = [["a", "b"],
                             ["c", "d", "e"]]
        String[][] goal = [["a", "b", "z"],
                           ["c", "d", "e"]]
        String defValue = "z"
        when:
        String[][] result = ArrayUtilsKt.INSTANCE.rectify(matrix, defValue)
        then:
        Arrays.deepEquals(goal, result)
    }

    // Testing denullify
    def "Denulling a two dimensional array."() {
        given:
        String[][] matrix = [["a", "b"],
                             [null, "d"]]
        String[][] goal = [["a", "b"],
                           ["z", "d"]]
        String defValue = "z"
        when:
        ArrayUtilsKt.INSTANCE.deNullify(matrix, defValue)
        then:
        Arrays.deepEquals(matrix, goal)
    }

    def "Denulling a two dimensional array with nulls in the first index"() {
        given:
        String[][] matrix = [["a", "b"],
                             null]
        String[][] goal = [["a", "b"],
                           []]
        String defValue = "z"
        when:
        ArrayUtilsKt.INSTANCE.deNullify(matrix, defValue)
        then:
        Arrays.deepEquals(matrix, goal)
    }

    // Testing cutFirstIndex
    def "Cutting a vector out of a fixed first index of an array"() {
        given:
        Integer[][] matrix = [[1, 2, 3, 4],
                              [5, 6, 7, 8],
                              [9, 1, 2, 3],
                              [4, 5, 6, 7]]
        int cut = 2
        Integer[][] goalMatrix = [[1, 2, 3, 4],
                                  [5, 6, 7, 8],
                                  [4, 5, 6, 7]]
        Integer[] goalVektor = [9, 1, 2, 3]
        when:
        ExtractedArray<Integer> extraction = ArrayUtilsKt.INSTANCE
            .cutFirstIndex(matrix, cut)
        then:
        Arrays.deepEquals(extraction.m_rest, goalMatrix)
        and:
        Arrays.equals(extraction.m_extracted, goalVektor)
    }

    def "Cutting an empty vector out of a fixed first index of an array"() {
        given:
        Integer[][] matrix = [[1, 2, 3, 4],
                              [5, 6, 7, 8],
                              [],
                              [4, 5, 6, 7]]
        int cut = 2
        Integer[][] goalMatrix = [[1, 2, 3, 4],
                                  [5, 6, 7, 8],
                                  [4, 5, 6, 7]]
        Integer[] goalVektor = []
        when:
        ExtractedArray<Integer> extraction = ArrayUtilsKt.INSTANCE
            .cutFirstIndex(matrix, cut)
        then:
        Arrays.deepEquals(extraction.m_rest, goalMatrix)
        and:
        Arrays.equals(extraction.m_extracted, goalVektor)
    }

    def "Cutting a to high index out of an arrays first index"() {
        given:
        Integer[][] matrix = [[1, 2, 3, 4],
                              [5, 6, 7, 8],
                              [9, 1, 2, 3],
                              [4, 5, 6, 7]]
        int cut = 6
        when:
        ExtractedArray<Integer> extraction = ArrayUtilsKt.INSTANCE
            .cutFirstIndex(matrix, cut)
        then:
        thrown(ArrayIndexOutOfBoundsException)
    }

    def "Cutting something out of a fixed first index of a nearly empty Array"() {
        given:
        Integer[][] matrix = [[]]
        int cut = 0
        Integer[][] goalMatrix = []
        Integer[] goalVektor = []
        when:
        ExtractedArray<Integer> extraction = ArrayUtilsKt.INSTANCE
            .cutFirstIndex(matrix, cut)
        then:
        Arrays.deepEquals(extraction.m_rest, goalMatrix)
        and:
        Arrays.equals(extraction.m_extracted, goalVektor)
    }

    def "Cutting something out of a fixed first index of nothing"() {
        // TODO groovy cannot create empty nothingness
    }

    // Testing cutSecondIndex
    def "Cutting a vector out of a fixed second index of an array"() {
        given:
        Integer[][] matrix = [[1, 2, 3, 4],
                              [5, 6, 7, 8],
                              [9, 1, 2, 3],
                              [4, 5, 6, 7]]
        int cut = 2
        Integer[][] goalMatrix = [[1, 2, 4],
                                  [5, 6, 8],
                                  [9, 1, 3],
                                  [4, 5, 7]]
        Integer[] goalVektor = [3, 7, 2, 6]
        when:
        ExtractedArray<Integer> extraction = ArrayUtilsKt.INSTANCE
            .cutSecondIndex(matrix, cut)
        then:
        Arrays.deepEquals(extraction.m_rest, goalMatrix)
        and:
        Arrays.equals(extraction.m_extracted, goalVektor)
    }

    def "Cutting a vector out of a fixed second index of a not rectangular array"() {
        given:
        Integer[][] matrix = [[1, 2, 3, 4],
                              [5, 6, 7, 8],
                              [],
                              [4, 5, 6, 7]]
        int cut = 2
        when:
        ExtractedArray<Integer> extraction = ArrayUtilsKt.INSTANCE
            .cutSecondIndex(matrix, cut)
        then:
        thrown(ArrayIndexOutOfBoundsException)
    }

    def "Cutting a vector out of a fixed second index of a not rectangular array 2"() {
        given:
        Integer[][] matrix = [[1, 2, 3, 4],
                              [5, 6, 7, 8],
                              [9, 0, 1],
                              [4, 5, 6, 7]]
        Integer[][] goalMatrix = [[1, 2, 4],
                                  [5, 6, 8],
                                  [9, 0],
                                  [4, 5, 7]]
        Integer[] goalVector = [3, 7, 1, 6]
        int cut = 2
        when:
        ExtractedArray<Integer> extraction = ArrayUtilsKt.INSTANCE
            .cutSecondIndex(matrix, cut)
        then:
        Arrays.deepEquals(extraction.m_rest, goalMatrix)
        and:
        Arrays.equals(extraction.m_extracted, goalVector)
    }

    def "Cutting a to high index out of an arrays second index"() {
        given:
        Integer[][] matrix = [[1, 2, 3, 4],
                              [5, 6, 7, 8],
                              [9, 1, 2, 3],
                              [4, 5, 6, 7]]
        int cut = 6
        when:
        ExtractedArray<Integer> extraction = ArrayUtilsKt.INSTANCE
            .cutSecondIndex(matrix, cut)
        then:
        thrown(ArrayIndexOutOfBoundsException)
    }

    def "Cutting something out of a fixed second index of a nearly empty Array"() {
        given:
        Integer[][] matrix = [[]]
        int cut = 0
        Integer[][] goalMatrix = []
        Integer[] goalVektor = []
        when:
        ExtractedArray<Integer> extraction = ArrayUtilsKt.INSTANCE
            .cutSecondIndex(matrix, cut)
        then:
        Arrays.deepEquals(extraction.m_rest, goalMatrix)
        and:
        Arrays.equals(extraction.m_extracted, goalVektor)
    }

    // Testing createNumberedArray
    def "CreateNumberedArray"(Locale locale, Double start, Double step, int
        subComma, int length, String[] goal) {
        when:
        String[] result = ArrayUtilsKt.INSTANCE.createNumberedArray(locale, start,
            step, subComma, length)
        then:
        Arrays.equals(goal, result)
        where:
        locale         | start | step | subComma | length | goal
        Locale.GERMAN  | 0.0d  | 1.0d | 0        | 5      | ["0", "1", "2", "3", "4"]
        Locale.GERMAN  | 1.0d  | 1.0d | 0        | 5      | ["1", "2", "3", "4", "5"]
        Locale.GERMAN  | 0.5d  | 0.5d | 0        | 5      | ["0", "1", "1", "2", "2"]
        Locale.GERMAN  | 0.5d  | 0.5d | 1        | 5      | ["0,5", "1,0", "1,5", "2,0", "2,5"]
        Locale.ENGLISH | 0.5d  | 0.5d | 1        | 5      | ["0.5", "1.0", "1.5", "2.0", "2.5"]
        Locale.ENGLISH | 0.5d  | 0.25d| 1        | 5      | ["0.5", "0.7", "1.0", "1.2", "1.5"]
        Locale.GERMAN  | 0.0d  | 0.0d | 0        | 5      | ["0", "0", "0", "0", "0"]
        Locale.GERMAN  | 0.0d  | 0.0d | 0        | 0      | []
        Locale.GERMAN  | 5.0d  |-1.0d | 0        | 5      | ["5", "4", "3", "2", "1"]
    }

    // Testing CreateNextNumberedArray
    def "CreateNextNumberedArray"(String penulimate, String last, int length, String[] goal) {
        when:
        String[] result = ArrayUtilsKt.INSTANCE.createNextNumberedArray(penulimate, last, length)
        then:
        Arrays.equals(goal, result)
        where:
        penulimate | last | length | goal
        "0"        | "0"  | 1      | ["0"]
        "0"        | "1"  | 1      | ["2"]
        "-1"       | "1"  | 1      | ["3"]
        "0"        | "-1" | 1      | ["-2"]
        "0"        | "1"  | 5      | ["2", "3", "4", "5", "6"]
        "0.5"      | "1.0"| 1      | ["1.5"]
        "0,5"      | "1,0"| 1      | ["1,5"]
    }

    // Testing CreateAlphaNumericArray
    def "CreateAlphaNumericArray"() {
        // Tested already in CreateSetArray
        // TODO should I write a test?
    }

    // Testing CreateSetArray
    def "CreateSetArray working"(int length, boolean neutral, boolean incr, ArrayList<Integer> startAt, char[] set, String[] goal) {
        when:
        String[] result = ArrayUtilsKt.INSTANCE.createSetArray(set, length, neutral, startAt, incr)
        then:
        Arrays.equals(result, goal)
        where:
        length | neutral | incr  | startAt             | set    | goal
        6      | true    | false | new START_AT_ZERO() | "0123" | ["0", "1", "2", "3", "10", "11"]
        6      | true    | false | new START_AT_ONE()  | "0123" | ["1", "2", "3", "10", "11", "12"]
        6      | false   | false | new START_AT_ZERO() | "0123" | ["0", "1", "2", "3", "00", "01"]
        6      | true    | true  | new START_AT_ZERO() | "0123" | ["1", "2", "3", "10", "11", "12"]
    }

    def "CreateSetArray not working"(int length, boolean neutral, boolean incr, ArrayList<Integer> startAt, char[] set) {
        when:
        ArrayUtilsKt.INSTANCE.createSetArray(set, length, neutral, startAt, incr)
        then:
        thrown(ArrayIndexOutOfBoundsException)
        where:
        length | neutral | incr  | startAt             | set
        6      | true    | false | new START_AT_ZERO() | ""
        6      | true    | false | new START_AT_ONE()  | "0"
    }

    // Testing CreateStringFromIndexSet
    def "CreateStringFromIndexSet"() {
        // Already tested by CreateSetArray which is generating its content with the help of this method
        // TODO should I write a test?
    }

    // Testing CreateIndexSetFromString
    def "CreateIndexSetFromString"(String content, char[] set, ArrayList<Integer> goal) {
        when:
        then:
        where:
        content | set    | goal
        "0"     | "0"    | ["0"]
        "0"     | "01"   | ["0"]
        "1"     | "01"   | ["1"]
        "1"     | "10"   | ["0"]
    }
}

